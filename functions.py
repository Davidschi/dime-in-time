import sys
import time
import os
import platform


def typewrite(message, input_mode=False, delay=0.05, newline_delay=0.5):
    time.sleep(newline_delay)
    for char in message:
        sys.stdout.write(char)
        sys.stdout.flush()
        if char == "\n":
            time.sleep(newline_delay)
        else:
            time.sleep(delay)
    if input_mode is True:
        return input()

def clear_terminal():
    if platform.system() == "Linux":
        os.system("clear")
    else:
        os.system("cls")
