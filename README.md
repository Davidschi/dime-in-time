# Dime In Time

A simple text adventure about climbing up the ranks of a world famous casino and getting rich using time traveling. <br />
Inspired by [Kakegurui](https://en.wikipedia.org/wiki/Kakegurui_%E2%80%93_Compulsive_Gambler) and [Zork](https://en.wikipedia.org/wiki/Zork).

![ ](https://i.imgur.com/tJfmxqs.png)