import functions


if __name__ == "__main__":
    functions.clear_terminal()
    functions.typewrite("O)))))                                     O))                O))) O))))))\n\
O))   O))  O)                              O))                     O))     O)\n\
O))    O))   O))) O)) O))    O))           O))O)) O))              O))       O))) O)) O))    O))\n\
O))    O))O)) O))  O)  O)) O)   O))        O)) O))  O))            O))    O)) O))  O)  O)) O)   O))\n\
O))    O))O)) O))  O)  O))O))))) O))       O)) O))  O))            O))    O)) O))  O)  O))O))))) O))\n\
O))   O)) O)) O))  O)  O))O)               O)) O))  O))            O))    O)) O))  O)  O))O)\n\
O)))))    O))O)))  O)  O))  O))))          O))O)))  O))            O))    O))O)))  O)  O))  O))))\n", False, 0.0005, 0.03)
    print("\n\n\n")
    functions.typewrite("Press ENTER to start your adventure of time travel and riches...", True, 0.02, 0.5)
    functions.clear_terminal()
    functions.typewrite('You open your eyes, to look up to a gargantuan Building. The lights are shimmering, billboards shining in full brightness, displaying and advertising products you have never before seen. The Casino is bustling, with people walking left and right. As you take a closer look, you notice the entrance to the casino labelled with a vivid red sign, exclaiming "Destino del Oro", rimmed with bright LEDs and gold plating to it. As you step up to the velvet red carpet leading up to the grand entrance, a security guard in a jet-black tuxedo steps up to you.', True)

